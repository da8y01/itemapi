# ItemApi

ItemApi es el lado back-end de la aplicación Items. La implementación se hizo usando .NET Core (version 7).

## Instalación y ensayo

Clonar el repositorio con el respectivo comando `git clone`, y correr la aplicación con Visual Studio Community o con el comando `dotnet run`.

## Probar los endpoints

Usando herramientas como [Swagger](https://swagger.io/), [http-repl](https://learn.microsoft.com/en-us/aspnet/core/web-api/http-repl/?view=aspnetcore-7.0), [Postman](https://www.postman.com/) ó [curl](https://terminalcheatsheet.com/guides/curl-rest-api) probar los diferentes endpoints, API's o servicios habilitados.

## ToDo

* Actualizar README.md
* Evaluar si implementar DTO es necesario.
* Implementar la conexión con RDBMS.