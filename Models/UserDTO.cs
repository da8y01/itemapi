namespace ItemApi.Models;

public class UserDTO
{
    public long Id { get; set; }
    public string? Name { get; set; }
    public string? Email { get; set; }
    public bool IsAdmin { get; set; }
}