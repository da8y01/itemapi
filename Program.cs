using Microsoft.EntityFrameworkCore;
using ItemApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
// var connectionString = Configuration.GetConnectionString("DefaultConnection");
var connectionString = "server=localhost; database=test; user=root";
builder.Services.AddDbContext<UserContext>(opt =>
    // opt.UseInMemoryDatabase("UserList"));
    opt.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();